// eslint-disable-next-line import/prefer-default-export
export const FILTER_STATES = {
  ALL: 'all',
  SYNCED: 'synced',
  PENDING: 'pending',
  FAILED: 'failed',
};
